import React, { Component } from 'react'; 
import Ninjas from './Ninjas';
import AddNinja from './AddNinja';

class App extends Component {

  state = {
    ninjas : [
        {name: 'imranffff', age: 19, designation:'app', id :1},
        {name: 'imranddderr', age:50, designation:'app2', id :2},
        {name: 'imranddd', age : 60, designation : 'app4', id :3}
    ]
  }

  addNinja = (ninja) => {
    // console.log(this.state);
    ninja.id = Math.random();

    let ninjas = [...this.state.ninjas, ninja] // user spared operator/ array copy

    this.setState({
      ninjas: ninjas
    })
   }

   deleteNinja = (id) => {
    console.log(id)
    let ninjas = this.state.ninjas.filter(ninja => {
      return ninja.id  !== id
      console.log('check', ninja.id !== id);
    });

    this.setState({
      ninjas : ninjas
    })

   }

  render() {
    return (
      <div className="App">
          <h1>My First React APP, Welcome</h1>
          <Ninjas deleteNinja={this.deleteNinja} ninjas={this.state.ninjas}/>      
          <AddNinja addNinja={this.addNinja}/>   
      </div>
    );
  }
}

export default App;
