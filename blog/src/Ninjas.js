import React from 'react'; 

const Ninjas = ({ninjas, deleteNinja}) => { 

       // console.log(props)

        //const{ninjas} = props;
        
        // if else  : Condition output
        // const ninjaList = ninjas.map(ninja => {

        //     if(ninja.age > 20){

        //         return (
        //             <div className="ninja" key={ninja.id}>
        //                 <div> {ninja.name }</div>
        //                 <div> { ninja.age }</div>
        //                 <div> { ninja.designation }</div>
        //             </div>
        //         )
        //     } else {
        //         return null;
        //     }
        // })
        
        //***/ Ternary oparetor using : Condition output
        const ninjaList = ninjas.map(ninja => {
            return ninja.age > 20 ? (
                     <div className="ninja" key={ninja.id}>
                         <div> {ninja.name }</div>
                         <div> { ninja.age }</div>
                         <div> { ninja.designation }</div>
                         <button onClick={()=> {deleteNinja(ninja.id)}}>Delete id {ninja.id}</button>
                          
                         {/* <button onClick={ deleteNinja(ninja.id)}>Delete</button> */}

                     </div>
            ) : null;
        })

        return (
           <div className="ninja-list">
               { ninjaList }
           </div>
        )
   
}
export default Ninjas