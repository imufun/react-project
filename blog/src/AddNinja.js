import React , {Component} from 'react';

class AddNinja extends React.Component {


    state = {
        name : null,
        age: null,
        designation: null
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);

       this.props.addNinja(this.state);
         
    }

    render(){
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                    <p>
                        <label htmlFor="name">Name</label>
                        <input type="text" id="name" onChange={this.handleChange}/>
                    </p>
                    <p>
                        <label htmlFor="age">age</label>
                        <input type="text" id="age" onChange={this.handleChange}/>
                    </p>
                    <p>
                        <label htmlFor="designation">designation</label>
                        <input type="text" id="designation" onChange={this.handleChange}/>
                    </p>
                    <p>
                        <button>Submit</button>
                    </p>
                </form>
            </div>
        )
    }
}

export default AddNinja