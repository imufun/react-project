import React, { Component } from 'react' 

import { Layout} from 'antd';
import { Row, Col } from 'antd';
import Notification from './Notification'
import ProjectList from '../project/ProjectList'

import {connect} from 'react-redux'

const { Content, Footer } = Layout;
export class Dashboard extends Component {
 

  render() {
    //console.log(this.props)
    const {projects} = this.props
    return (
      <div>
        <Layout className="layout">
            <Content className="container">
                 <Row>
                    <Col span={12}><ProjectList projects={projects}/></Col>
                    <Col span={12}><Notification/></Col>
                </Row>
            </Content>      
            <Footer style={{ textAlign: 'center' }}>
                Ant Design ©2018 Created by Ant UED
            </Footer>      
        </Layout>
      </div>
    )
  }
}

  const  mapStateToProps = (state) => {
  return {
    projects : state.project.projects
  }
}

export default connect(mapStateToProps)(Dashboard)
