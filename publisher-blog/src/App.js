import React, { Component } from 'react'; 
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom'

import Navbar from './layout/Navbar'
import Dashboard from './dashboard/Dashboard' 
import ProjectDetails from './project/ProjecDetails'
import SignIn from './auth/SignIn'
import SignUP from './auth/SignUp'
import CreatePoject from './project/CreateProject'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
           <Navbar/>
           <Switch>
              <Route exact path='/' component={Dashboard} />
              <Route path='/project/:id' component={ProjectDetails} />
              <Route path='/signin' component={SignIn}/>
              <Route path='/signup' component={SignUP}/>
              <Route path='/create-project' component={CreatePoject}/>
           </Switch> 
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
