import React from 'react'
import PorjectSummary from './PorjectSummary'
const ProjectList = ({projects}) => {
  return (
    <div>
       {projects && projects.map(project =>{
          return (
              <PorjectSummary project={project} key={project.id}/>
            )          
       })}
    </div>
  )
}

export default ProjectList
