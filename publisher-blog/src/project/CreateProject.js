import React, { Component } from 'react'
import { connect } from 'react-redux'
import {createProject} from '../store/actions/projectAction'

class CreateProject extends Component {

  state = {
    title : '',
      description: ''
  }


  handleSubmit = (e) => {
      e.preventDefault();
     // console.log(this.state)
     this.props.createProject(this.state)
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id] : e.target.value
    })
  }

  render() {


    return (
      <div className="container">
        <div className="row mt-5">
            <div className="col-5 mx-auto">
              <form onSubmit={this.handleSubmit}>
                  <div className="from-group mb-2">
                      <label htmlFor="">Title </label>
                      <input type="text" className="form-control" placeholde="Title" onChange={this.handleChange} id="title"/>                    
                  </div>
                  <div className="from-group mb-4">
                      <label htmlFor="">Descrption</label>
                      <textarea type="text" placeholde="Description"  className="form-control" onChange={this.handleChange} id="descrition"/>                    
                  </div>
                  <div className="from-group text-center">
                      <button className="btn btn-primary">Create</button>                       
                  </div>
              </form>   
            </div>     
        </div>
      </div>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    createProject : (project) => dispatch(createProject(project))
  }
}

export default connect(null, mapDispatchToProps )(CreateProject)
