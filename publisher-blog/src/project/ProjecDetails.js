import React from 'react'

const ProjecDetails = (props) => {
 // console.log(props);
  const id = props.match.params.id;
  return (
    <div className="container">
       <div className="row mt-5">
          <div className="col">
              <h2>Project Title - {id}</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium delectus quasi commodi quos hic fugiat at, quae quaerat suscipit neque beatae ex vel, aspernatur, tenetur earum perspiciatis a deleniti est!</p>
              <hr/>
              <p>Posted By The Sinbad</p>
              <p>Date:Dec 18,2018</p>
          </div>
       </div>
    </div>
  ) 
}

export default ProjecDetails
