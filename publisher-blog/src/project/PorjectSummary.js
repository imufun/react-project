import React from 'react'
import { Card } from 'antd';
const PorjectSummary = ({project}) => { 
 
  return (
    <div>
        <Card className ="mb-2"
            title="Card title"
            extra={<a href="#">More</a>}
            style={{ width: 300 }}>
            <p>{project.title}</p>
            <p>{project.content}</p>
            <p className="text-gray">Date: 10,2018 </p> 
         </Card>
    </div>
  )
}

export default PorjectSummary
