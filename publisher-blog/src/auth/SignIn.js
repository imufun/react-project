import React, { Component } from 'react'
 
    class SignIn extends Component {

    state = {
        email: '',
        password: ''
    }
    handleSubmit = (e) => {

        e.preventDefault();
        console.log(this.state)
    }

    handleChange = (e) => {
    console.log('e');
        this.setState({
            [e.target.id]: e.target.value
        })

    }
  render() {
    return (
      <div className="container">
            <div className="row mt-5">
                <div className="col-5  justify-content-center mx-auto">
                    <p>Sign In</p>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group"> 
                     <label htmlFor="email">Email address</label>
                     <input id="email" onChange={this.handleChange} type="email" className="form-control"   placeholder="Enter email"/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input id="password" onChange={this.handleChange} type="password" className="form-control"  placeholder="Password"/>
                    </div>
                    {/* <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1"/>
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div> */}
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
      </div>
    )
  }
}

export default SignIn
