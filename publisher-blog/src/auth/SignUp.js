import React, { Component } from 'react'

  
    class SignUp extends Component {

    state = {
        email: '',
        password: '',
        firstName: '',
        lastName: ''
    }
    handleSubmit = (e) => {

        e.preventDefault();
        console.log(this.state)
    }

    handleChange = (e) => {
    console.log('e');
        this.setState({
            [e.target.id]: e.target.value
        })

    }
  render() {
    return (
      <div className="container">
            <div className="row mt-5">
                <div className="col-5  justify-content-center mx-auto">
                    <p>Sign Up</p>
                <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                     <label htmlFor="firstName">First Name</label>
                     <input id="firstName" onChange={this.handleChange} type="text" className="form-control"   placeholder="Enter email"/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                     <label htmlFor="lastName">Last Name</label>
                     <input id="lastName" onChange={this.handleChange} type="text" className="form-control"   placeholder="Enter email"/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                     <label htmlFor="email">Email address</label>
                     <input id="email" onChange={this.handleChange} type="email" className="form-control"  placeholder="Enter email"/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input id="password" onChange={this.handleChange} type="password" className="form-control"   placeholder="Password"/>
                    </div>
                     
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
                </div>
            </div>
      </div>
    )
  }
}

export default SignUp
