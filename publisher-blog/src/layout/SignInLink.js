import React from  'react'
import {NavLink} from 'react-router-dom'
import { Menu,Icon} from 'antd';

const SingInLink = () => {

    return (
   <div className="float-right">
   <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['2']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1"><NavLink to="/create-project">New Project</NavLink></Menu.Item>
        <Menu.Item key="2"><NavLink to="/">Logout</NavLink></Menu.Item>
        <Menu.Item key="3"><NavLink to="/"><Icon type="user" /></NavLink></Menu.Item>
      </Menu>
   </div>
    )
}

export default SingInLink