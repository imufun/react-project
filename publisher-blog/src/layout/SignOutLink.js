import React from  'react'
import {NavLink} from 'react-router-dom'   

import { Menu} from 'antd';


const SignOutLink = () => {

    return (
   <div className="float-right"> 
        <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['2']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1"><NavLink to="/signin">Sigin</NavLink></Menu.Item>
        <Menu.Item key="2"><NavLink to="/signup">Signup</NavLink></Menu.Item> 
      </Menu>
   </div>
    )
}

export default SignOutLink