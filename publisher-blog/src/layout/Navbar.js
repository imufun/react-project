import React from 'react'   
import {Link} from 'react-router-dom'
import { Layout} from 'antd';
import SingInLink from './SignInLink'
import SignOutLink from './SignOutLink'

const { Header} = Layout; 

const NavBar = () => {
    return (

<div>
<Layout className="layout">
    <Header>
        <Link to='/' className="float-left">Logo</Link>
        <SingInLink/>
        <SignOutLink/>
    </Header>
    </Layout>
</div> 
    )
}

export default NavBar