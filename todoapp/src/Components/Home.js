import React , {Component} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'

class Home extends Component {

    state = {
        posts : []
    }
    
     componentDidMount (){
         axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(res => {
                    console.log(res)
                    this.setState({
                        posts: res.data.slice(0,10)
                    })
                }
            )
     }


    render(){

        const { posts} = this.state;
        const postList = posts.length ? (
           posts.map(post => {
               return (
                <div className="PostData " key={post.id}> 
                    <div className="card">
                        <div className="card-body">
                            <Link to={'/' + post.id}>{post.title}</Link> 
                            <p className="card-text">{ post.body}</p>
                        </div> 
                    </div>                    
                </div>
               )
           })
        ) : (
            <div className="alert-warning">
                <p>You have no post</p>
            </div>
        )

        return (
            <div className="Home-page">
                {postList}
            </div>
        )
    }
}
export default Home