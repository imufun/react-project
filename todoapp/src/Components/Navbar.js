import React , {Component} from 'react'
import {Link, NavLink, withRouter} from 'react-router-dom'


const Navbar = (props) => {

    // setTimeout ( ()=> {
    //     props.history.push('/About');

    // },2000)


    return (
        <div className="navbar-page">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/Home">Home <span className="sr-only">(current)</span></NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/About">About</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/Blog">Blog</NavLink>
                    </li> 
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/Contact">Contact</NavLink>
                    </li>                   
                </ul>
            </div>
            </nav>
        </div>
    )
}
export default withRouter(Navbar)