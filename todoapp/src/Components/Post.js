import React, {Component} from 'react'
import axios from 'axios'
class Post extends Component {

    state = {
        post: null
    }

    componentDidMount(){
        let id = this.props.match.params.post_id; 
        axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
            .then(res => {
                this.setState({
                       post : res.data 
                })
                console.log('ff',res)
            })         
    }   
    
    render () {       


        const post = this.state.post ? (
            <div className="post-data">
                <h3 className="text-center">
                    {this.state.post.title}
                </h3>
                <div className="body">
                    {this.state.post.body}
                </div>
            </div>
        ) : (
            <div className="center">Loading...</div>
        )

        return (
            <div className="postData">
                {post}
            </div>
        )
    }
}
export default Post