import React, { Component } from 'react';
import Todo from './Todo'
import AddTodo from './AddTodo'
import Navbar from './Components/Navbar'
import Home from './Components/Home'
import About from './Components/About'
import Contact from './Components/Contact'
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import Post from './Components/Post'

class App extends Component {

    state = {
      todos : [
        {id: 1, content : 'buy some milk'},
        {id: 2, content : 'buy some cow'} 
      ]
    }


    deteleItem = (id) => {
      console.log(id)
      const todos = this.state.todos.filter(todo => {
        return todo.id !== id        
      })
      this.setState({
        todos :  todos
      })
    }

    addTodo = (todo) => {
      todo.id = Math.random()

       let todos = [...this.state.todos, todo]
       
       this.setState({
          todos : todos
       })
    }


  render() {
    return (
      <BrowserRouter>
        <div className="App">
            <Navbar/>
            <Switch>
                <Route path='/home' component ={Home}/>
                <Route path='/about' component ={About}/>
                <Route path='/Contact' component ={Contact}/>
                <Route path='/:post_id' component ={Post}/>
            </Switch>
            <div className="container mt-5">             
                <div className="row">
                    <div className="col-12">
                        <h1 className="text-center text-success">Todo's</h1>
                    </div>
                </div>
              <AddTodo addTodo={this.addTodo}/>
              <Todo  todos={this.state.todos} deteleItem={this.deteleItem} />
            </div> 
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
