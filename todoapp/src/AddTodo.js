import React , {Component} from 'react'

class AddTodo extends React.Component {

    state = {
        content: null
    }
    handleChange = (e) => {
        this.setState({
            content: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        this.props.addTodo(this.state)
    }

    render(){
        return (
            <div className="todo-form">
                <form onSubmit={this.handleSubmit}>
                    <div className="row mb-3">
                        <div className="col text-center card py-3">
                            <p>Add New Todo</p>
                            <input type="text" className="form-control" placeholder="Enter Something" onChange={this.handleChange}/>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
export default AddTodo