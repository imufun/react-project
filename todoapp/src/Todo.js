import React from 'react'

const Todo = ({todos, deteleItem}) => {

    const todoList = todos.length ? (
        todos.map(todos => {
            return (
                <div className="todo-app" key={todos.id}>
                    <ul className="list-group" >
                        <li className="list-group-item mb-1">                           
                            {todos.content}
                            <button type="button" className="btn btn-outline-danger float-right" onClick={()=> {deteleItem(todos.id)}}>
                                Delete
                            </button>
                        </li> 
                    </ul>
                </div>
            )
        })
    ) : ( 
        <div className="alert alert-danger text-center" role="alert">
            You have no todo's 
        </div>
    ) 


    return (

        <div className="todo">
            {todoList}
        </div>
    )
}

export default Todo